import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrimengComponent } from './primeng/primeng.component';
import { BootstrapComponent } from './bootstrap/bootstrap.component';

const routes: Routes = [
  { path: 'primeng', component: PrimengComponent },
  { path: 'bootstrap', component: BootstrapComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
