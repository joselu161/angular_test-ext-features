import { Component, OnInit } from '@angular/core';
import { TestService } from './test.service';
import { DataService } from './data.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss'],
})
export class TestComponent implements OnInit {
  user: {
    name: string;
  };
  isLoggedIn = false;

  data: string;

  constructor(
    private testService: TestService,
    private dataService: DataService
  ) {}

  ngOnInit(): void {
    this.user = this.testService.user;
    this.dataService.getDetails().then(data => (this.data = data));
  }
}
