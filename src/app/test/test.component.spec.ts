import {
  async,
  ComponentFixture,
  TestBed,
  fakeAsync,
  tick,
} from '@angular/core/testing';

import { TestComponent } from './test.component';
import { DataService } from './data.service';
import { TestService } from './test.service';

describe('TestComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TestComponent],
    });
  });

  it('chequeamos si el componente se genera correctamente', () => {
    const fixture = TestBed.createComponent(TestComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('debemos poder capturar el nombre de usuario del servicio', () => {
    const fixture = TestBed.createComponent(TestComponent);
    const app = fixture.debugElement.componentInstance;
    // inyectamos el servicio al componente
    const userService = fixture.debugElement.injector.get(TestService);
    // detectamos los cambios que se producen tras la inserción (sino, no funciona)
    fixture.detectChanges();
    expect(userService.user.name).toEqual(app.user.name);
  });

  it('chequeamos si el nombre del usuario aparece si el usuario está logueado', () => {
    const fixture = TestBed.createComponent(TestComponent);
    const app = fixture.debugElement.componentInstance;
    app.isLoggedIn = true;
    // detectamos los cambios que se producen tras la inserción (sino, no funciona)
    fixture.detectChanges();
    // vamos a compilar el componente
    const compiled = fixture.debugElement.nativeElement;
    console.log(compiled.querySelector('p').textContent);
    expect(compiled.querySelector('p').textContent).toContain(app.user.name);
  });

  it('chequeamos que el nombre del usuario no aparece si el usuario no está logueado', () => {
    const fixture = TestBed.createComponent(TestComponent);
    const app = fixture.debugElement.componentInstance;
    app.isLoggedIn = false;
    // detectamos los cambios que se producen tras la inserción (sino, no funciona)
    fixture.detectChanges();
    // vamos a compilar el componente
    const compiled = fixture.debugElement.nativeElement;
    console.log(compiled.querySelector('p').textContent);
    expect(compiled.querySelector('p').textContent).not.toContain(
      app.user.name
    );
  });

  it('no debería devolver los datos correctamente si no realizamos una llamada asincrona', () => {
    const fixture = TestBed.createComponent(TestComponent);
    const app = fixture.debugElement.componentInstance;
    const dataService = fixture.debugElement.injector.get(DataService);
    // hacemos esto para emular la respuesta de los datos que vamos a recibir de DATA (supuestamente 'Data', que sería el MOCK)
    const spy = spyOn(dataService, 'getDetails').and.returnValue(
      Promise.resolve('Data')
    );
    fixture.detectChanges();
    expect(app.data).toBe(undefined);
  });

  it('debería devolver los datos correctamente al realizar el test de forma asíncrona', async(() => {
    const fixture = TestBed.createComponent(TestComponent);
    const app = fixture.debugElement.componentInstance;
    const dataService = fixture.debugElement.injector.get(DataService);
    // hacemos esto para emular la respuesta de los datos que vamos a recibir de DATA (supuestamente 'Data', que sería el MOCK)
    const spy = spyOn(dataService, 'getDetails').and.returnValue(
      Promise.resolve('Jamon')
    );
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(app.data).toBe('Jamon');
    });
  }));

  it('debería devolver los datos correctamente al realizar el test de forma asíncrona (OTRA FORMA DE HACERLO)', fakeAsync(() => {
    const fixture = TestBed.createComponent(TestComponent);
    const app = fixture.debugElement.componentInstance;
    const dataService = fixture.debugElement.injector.get(DataService);
    // hacemos esto para emular la respuesta de los datos que vamos a recibir de DATA (supuestamente 'Data', que sería el MOCK)
    const spy = spyOn(dataService, 'getDetails').and.returnValue(
      Promise.resolve('Jamon')
    );
    fixture.detectChanges();
    tick();
    expect(app.data).toBe('Jamon');
  }));
});
